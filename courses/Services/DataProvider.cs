﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;

namespace courses.Infrastructure
{
    class DataProvider : IDataProvider
    {
        private readonly IMemoryCache _memoryCache;

        public DataProvider(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }


        public async Task<Courses> GetData()
        {
            return await _memoryCache.GetOrCreateAsync("courses", async cacheEntry =>
            {
                var json = await File.ReadAllTextAsync("./Data/data.json");
                return JsonConvert.DeserializeObject<Courses>(json);
            });
        }
    }
}