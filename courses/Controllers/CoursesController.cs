﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using courses.Infrastructure;
using Microsoft.AspNetCore.Cors;
using Newtonsoft.Json;

namespace courses.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CoursesController : ControllerBase
    {
        private readonly IDataProvider _dataProvider;

        public CoursesController(IDataProvider dataProvider)
        {
            _dataProvider = dataProvider;
        }

        [HttpGet]
        [Route("search")]
        public async Task<IActionResult> Get()
        {
            Courses courses = await _dataProvider.GetData();

            return Ok(JsonConvert.SerializeObject(courses.Languages));
        }



        [HttpGet]
        [Route("search/{languageId}")]
        public async Task<IActionResult> Get(int languageId)
        {
            Courses courses = await _dataProvider.GetData();

            var data = courses.Lecturers.Where(c => c.Languages.Any(f => f == languageId))
                .Select(l => new { Id = l.Id, Name = l.Name, Email = l.Email, Lang = l.Languages.Join(courses.Languages, lid => lid, l => l.Id, (f, g) => new { lang = g }) });

            return Ok(JsonConvert.SerializeObject(data, Formatting.Indented));
        }

    }
}
