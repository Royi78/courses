﻿using System.Threading.Tasks;

namespace courses.Infrastructure
{
    public interface IDataProvider
    {
        public Task<Courses> GetData();
    }
}