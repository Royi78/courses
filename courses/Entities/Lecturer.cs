﻿using System.Collections.Generic;


    public class Lecturer
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public List<int> Languages { get; set; }
    }
