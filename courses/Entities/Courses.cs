﻿using System.Collections.Generic;

 
    public class Courses
    {
        public List<Lecturer> Lecturers { get; set; }
        public List<Language> Languages { get; set; }
    }
 